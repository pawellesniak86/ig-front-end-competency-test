//creating namespace:
var SwApiTest = SwApiTest || {}

SwApiTest.AppManager = class {

	constructor(parentElementId, apiUrl) {
		this.parentElement = document.getElementById(parentElementId);
		this.apiUrl = apiUrl;
	}

	init() {
		this.showSpinner();
		this.fetchData(SwApiTest.getData, this.apiUrl, data => {
		 	this.data = data;
		 	this.render();
		});
	}

	render() {
		this.hideSpinner();
		this.parentElement.innerHTML = "";
		this.renderTable(this.data);
		this.renderPagination();
	}

	renderTable(data) {
		this.table = new SwApiTest.SwCharsTable(this.parentElement, data.results, this.onTableItemClickCallback.bind(this));
		this.table.render();		
	}

	onTableItemClickCallback(id) {
		this.showSpinner();
		this.fetchData(SwApiTest.getData, this.apiUrl + "/" + id, data => {
		 	this.data = data;
			this.renderCharacterDetailsPage(this.data);
		});
	}

	renderCharacterDetailsPage(data) {
		this.hideSpinner();		
		this.toggleTableAndPagination();
		this.characterDetailsPage = new SwApiTest.SwCharsDetailsPage(this.parentElement, data, this.onBackButtonClickCallback.bind(this));
		this.characterDetailsPage.render();
		this.fetchCharacterFilms(data, this.characterDetailsPage);
	}

	onBackButtonClickCallback() {
		this.parentElement.querySelector(".character-data-container").innerHTML = "";
		this.toggleTableAndPagination();
	}

	renderPagination() {
		this.pagination = new SwApiTest.SwCharsPagination(this.parentElement, this.paginationButtonClickCallback.bind(this), this.data.next, this.data.previous);
		this.pagination.render();		
	}

	paginationButtonClickCallback(url) {
		this.showSpinner();
		this.fetchData(SwApiTest.getData, url, data => {
		 	this.data = data;
		 	this.render();
		});
	}

	showSpinner() {
		this.parentElement.classList.add("spinner");
		this.parentElement.classList.add("lesser-opacity");
	}

	hideSpinner() {
		this.parentElement.classList.remove("spinner");
		this.parentElement.classList.remove("lesser-opacity");
	}	

	fetchData(serviceProvider, apiUrl, onResolveCallback) {
		serviceProvider("GET", apiUrl).then(data => {
			onResolveCallback(data);			
		}).catch (err => {
			this.alertError(err);
		});
	}

	toggleTableAndPagination() {		
		if (this.table) {
			this.table.parentElement.querySelector(".main-container").classList.toggle("d-none");
		}
		if (this.pagination) {
			this.table.parentElement.querySelector(".pagination-container").classList.toggle("d-none");
		}
	}

	fetchCharacterFilms(data, characterDetailsPage) {
	 	this.showSpinner();
	 	let pendingPromises = [];
		data.films.forEach(film => {
			pendingPromises.push(SwApiTest.getData("GET", film));	
		});		
	
		Promise.all(pendingPromises).then(values => {
			this.hideSpinner();
			let filmTitles = [];
			for (let value of values) {
				filmTitles.push(value.title);
			}
			characterDetailsPage.setFilmData(filmTitles);
		});
	}

	alertError(err) {
		this.errorPage = new SwApiTest.SwCharsErrorPage(this.parentElement, err, "Something went wrong!");
		this.hideSpinner();
		this.errorPage.render();
		this.toggleTableAndPagination();
	}

}

SwApiTest.SwCharsTable = class {
	
	constructor(parentElement, items, onTableItemClickCallback) {		
		this.parentElement = parentElement;		
		this.items = items;
		this.onTableItemClickCallback = onTableItemClickCallback;
		this.template = `
		    <div class="main-container container-fluid">		      
		      <table class="table table-bordered table-hover results">
		        <thead class="thead-dark">
		          <tr>
		            <th scope="col" style="width: 5%">ID</th>
		            <th scope="col" style="width: 60%">Name</th>
		            <th scope="col" style="width: 17.5%">Height</th>
		            <th scope="col" style="width: 17.5%">Gender</th>
		          </tr>
		        </thead>
		        <tbody>
		        </tbody>
		      </table>		      
		      <div class="pagination-container"></div>    
		    </div>
		    <div class="character-data-container"></div>
		    <div class="error-container"></div>
		`;	
	}

	render() {
		this.parentElement.innerHTML = this.template;
		this.items.forEach(item => {
			let tableBody = this.parentElement.querySelector("tbody");
			let tableItem = new SwApiTest.SwCharsTableItem(tableBody, item, this.onTableItemClickCallback);
			tableItem.render();
		});
	}

}

SwApiTest.SwCharsTableItem = class {
	
	constructor(parentElement, data, onTableItemClickCallback) {	
		this.parentElement = parentElement;	
		this.data = data;
		this.onTableItemClickCallback = onTableItemClickCallback;
		this.id = SwApiTest.Helpers.getIdFromUrl(this.data.url);

		this.template = `
			<th scope="row">${this.id}</th>
			<td>${data.name}</td>
			<td>${data.height}</td>
			<td>${data.gender}</td>
		`;	
	}

	render() {
		this.tableItemElement = document.createElement("tr");
		this.tableItemElement.innerHTML = this.template;
		this.tableItemElement.addEventListener("click", this.clickCallback.bind(this));
		this.parentElement.appendChild(this.tableItemElement);
	}

	clickCallback() {
		this.onTableItemClickCallback(this.id);
	}

}

SwApiTest.SwCharsPagination = class {
	constructor(parentElement, paginationButtonClickCallback, next, previous) {
		this.parentElement = parentElement;
		this.paginationButtonClickCallback = paginationButtonClickCallback;
		this.next = next;
		this.previous = previous;

		this.template = `
	      <nav aria-label="...">
	        <ul class="pagination">
	          <li ${this.previous ? 'class="previous page-item"' : 'class="previous page-item disabled"'}>
	            <a class="page-link" href="#" ${this.previous ? '' : 'tabindex="-1"'}>Previous</a>
	          </li>
	          <li ${this.next ? 'class="next page-item"' : 'class="next page-item disabled"'}>
	            <a class="page-link" href="#" ${this.next ? '' : 'tabindex="-1"'}>Next</a>
	          </li>
	        </ul>
	      </nav>
		`;
	}

	render() {
		this.parentElement.querySelector(".pagination-container").innerHTML = this.template;
		if (this.next) {
			this.parentElement.querySelector(".next").addEventListener("click", this.onNextButtonClickCallback.bind(this));	
		}		
		if (this.previous) {
			this.parentElement.querySelector(".previous").addEventListener("click", this.onPreviousButtonClickCallback.bind(this));
		}		
	}

	onNextButtonClickCallback() {
		this.paginationButtonClickCallback(this.next);
	}

	onPreviousButtonClickCallback() {
		this.paginationButtonClickCallback(this.previous);
	}	

}

SwApiTest.SwCharsDetailsPage = class {
	
	constructor(parentElement, data, onBackButtonClickCallback) {
		this.parentElement = parentElement;
		this.data = data;
		this.onBackButtonClickCallback = onBackButtonClickCallback;
	
		this.template = `
			<div class="jumbotron jumbotron-fluid">
			  <div class="container">
			    <h1 class="display-4 text-center">${data.name}</h1>
			    <h3><b>Height: </b>${data.height}</h3>
			    <h3><b>Gender: </b>${data.gender}</h3>
			   	<h3><b>Movies:</h3>
			    <p class="lead">
			    	<ul class="movies-container list-group"></ul>	
			    </p>
			  	<button type="button" class="btn btn-primary back">Back</button>
			  </div>
			</div>
		`;
	}

	render() {
		this.parentElement.querySelector(".character-data-container").innerHTML = this.template;
		this.parentElement.querySelector(".back").addEventListener("click", this.onBackButtonClickCallback);	
	}

	setFilmData(filmTitles) {
		filmTitles.forEach(filmTitle => {
			this.parentElement.querySelector(".movies-container").innerHTML += `<li class="list-group-item">${filmTitle}</li>`;
		})
	}	

}

SwApiTest.SwCharsErrorPage = class {
	constructor(parentElement, err, errorHeaderText) {
		this.parentElement = parentElement;
		this.err = err;
		this.errorHeaderText = errorHeaderText;
		this.errorMessage = "An error occured... ";			
		if (this.err.status) {
			this.errorMessage += "STATUS: " + this.err.status + ".";
		}	
		if (this.err.statusText) {
			this.errorMessage += " " + this.err.statusText;
		}			
	
		this.template = `
			<div class="jumbotron jumbotron-fluid">
			  <div class="container">
			    <h1 class="display-4 text-center">${this.errorHeaderText}</h1>
			    <p class="lead">
			    	${this.errorMessage} Try refreshing the page.	
			    </p>
			  </div>
			</div>
		`;
	}

	render() {
		this.parentElement.querySelector(".error-container") ? this.parentElement.querySelector(".error-container").innerHTML = this.template : this.parentElement.innerHTML = this.template;		
	}

}

SwApiTest.Helpers = {}
SwApiTest.Helpers.getIdFromUrl = function(url) {
	return url.split("people/").pop().replace("/", "");
}