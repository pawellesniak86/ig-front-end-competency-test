# README #

Kilka słów komentarza odnośnie rozwiązanego zadania.

Rozwiązaniem jest widżet, który można instancjonować. Widżet zawiera w sobie komponenty ulepszające UX takie jak loader oraz notyfikacje o błędach. W pliku index.html zostały zainicjowane cztery instancje widżeta, z czego jedna z błędnym linkiem, w celu pokazania jak wygląda powiadomienie o błędzie. Kod jest napisany bez użycia jakichkolwiek frameworków i wykorzystuje składnię ES6. Do stylowania, z wyjątkiem kilku customowych klas, użyłem Bootstrap'a 4.

Paweł Leśniak